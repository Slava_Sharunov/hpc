/* ********** ********** ********** ********** ********** ********** **********
// HPC - exercise 8
// Dennis Madsen
// Wave stencil calculation - parallel stuff
********** ********** ********** ********** ********** ********** ********** */
use dim_input; //it contains input parameters
use Time;
use Math;
use BlockDist;
use Memory;

const PI: real = 3.1415926535;

//
// Program parameters; can be specified on the command line
//
config const
    // domain size
    x_max = MAX_X+4,
    y_max = x_max,
    z_max = x_max,

    // number of timesteps
    t_max = 10, //read from project

    // write result to files?
    write_output: bool = false;



proc write_data (u: [0..x_max, 0..y_max, 0..z_max] real, t)
{
    // create the file name
    var filename = t + ".txt";
    if (t < 10) then
        filename = "000" + filename;
    else if (t < 100) then
        filename = "00" + filename;
    else if (t < 1000) then
        filename = "0" + filename;


    //Create and open an output file with the specified filename in write mode
    var outfile = open(filename, iomode.cw);
    var writer = outfile.writer();

    // write the data
    // can be plotted with gnuplot: 'splot "./0001.txt" with pm3d'
    writer.write (u[.., .., z_max / 3 .. z_max / 3]);

    //close the file
    writer.close();
    outfile.close();
}

//
// Wave stencil benchmark.
//
proc main ()
{
	const
	    fMin: real = -1.0,
	    fMax: real = 1.0,
	    dx: real = (fMax - fMin) / ((x_max + 1) : real),
	    dt: real = dx / (2.0),
	    dt_dx_sq: real = (dt * dt) / (dx * dx),

//	    c1: real = 2.0,
//	    c2: real = -7.5,
//	    c3: real = (4.0 / 3.0),
//	    c4: real = (1.0 / 12.0);
        c1: real = 2.0 - dt_dx_sq * 7.5,
        c2: real = dt_dx_sq * 4.0 / 3.0,
        c3: real = dt_dx_sq * (-1.0/12.0),

        bigD = {0..x_max,0..y_max,0..z_max} dmapped Block({0..x_max,0..y_max,0..z_max}),
        smallD = bigD[2..x_max-2,2..y_max-2,2..z_max-2];
        //alternative definition
        //dom: domain(3) dmapped Block(boundingBox={0..x_max,0..y_max,0..z_max})={0..x_max,0..y_max,0..z_max},
        //sdom: subdomain(dom) = {2..x_max-2,2..y_max-2,2..z_max-2};

    // create grids
    var u_m1, u_0, u_1, tmp: [bigD] real = 0;
//    var u_m1, u_0, u_1: [0..x_max*y_max*z_max] real = 0;

        //u_m1: [0..x_max, 0..y_max, 0..z_max] //TODO
        //u_0: [0..x_max, 0..y_max, 0..z_max] //TODO
        //u_1: [0..x_max, 0..y_max, 0..z_max] //TODO

    // iterate on the grid and initialize using Chapel
    //TODO like in the C code
    forall (i,j,k) in smallD{
        var x:real = (i - 1) * dx + fMin;
        var y:real = (j - 1) * dx + fMin;
        var z:real = (k - 1) * dx + fMin;

        u_0[i,j,k] =  (sin(2 * PI * x) * sin(2 * PI * y) * sin(2 * PI * z));
    }

    u_m1 = u_0;

    // write the initial grid (a slice of it) to a file
    if (write_output) then
        write_data (u_0, 0);

    // do the stencil computation
    var clock = new Timer ();
    on Locales[0] {
    clock.start ();
    }

    //start the actual computation here
    for t in 1 .. t_max
    {
        //iterate on the grid elements and update the value of u_1
        //TODO
        forall (i,j,k) in smallD {
            u_1[i,j,k] =  c1 * u_0[i,j,k] - u_m1[i,j,k]
                + c2 * (
                    u_0[i+1,j,k] + u_0[i-1,j,k]+
                    u_0[i,j+1,k] + u_0[i,j-1,k]+
                    u_0[i,j,k+1] + u_0[i,j,k-1])
                + c3 * (
                    u_0[i+2,j,k] + u_0[i-2,j,k]+
                    u_0[i,j+2,k] + u_0[i,j-2,k]+
                    u_0[i,j,k+2] + u_0[i,j,k-2]);
        }

        if (write_output) then
            write_data (u_1, t);

        //swap grids
        tmp  = u_m1;
        u_m1 = u_0;
        u_0  = u_1;
        u_1  = tmp;
    }
    on Locales[0] {
    clock.stop ();
    write_data (u_0, 100);
    // print performance statistics
    var fFlops: real = (MAX_X : real) * (MAX_X : real) * (MAX_X : real) * (t_max : real) * 16.0; //check if your flops are 19
    writeln ("FLOPs in stencil code:      ", fFlops);
    writeln ("Time spent in stencil code: ", clock.elapsed ());
    writeln ("Performance in GFlop/s:     ", fFlops / (1e9 * clock.elapsed ()));
    //writeln ("Performance in GFlop/s:     ", 2.0);
    }

}
