use BlockDist;

config var n = 10;
var RD: domain(3) = {1..n, 1..n, 1..n};
writeln(RD);
var RDbigger = RD.expand((1,1,1));
writeln(RDbigger);
var RDsmaller = RD.expand((-1,-1,-1));
writeln(RDsmaller);

var RDext_p = RD.exterior(1,1,1);
writeln(RDext_p);
var RDext_n = RD.exterior((-1,-1,-1));
writeln(RDext_n);

var RDtrans_p = RD.translate((1,1,0));
writeln(RDtrans_p);
var RDtrans_n = RD.translate((-1,-1,-1));
writeln(RDtrans_n);

var RSD1, RSD2 : subdomain(RD);

// A subdomain is initially empty.
writeln("RSD1:", RSD1);
writeln("RSD2:", RSD2);

RSD1 = RD[..n/2, .., ..]; // This gives half of the domain
RSD2 = RD[n/2+1.., .., ..]; // And this the other half.

writeln("RSD1:", RSD1);
writeln("RSD2:", RSD2);

writeln(1 % 1);

//var D: domain(3) dmapped Block(boundingBox=[2..10-3,2..10-3,2..10-3])= [0..10, 0..10, 0..10];
//var innerD=D[2..10-3,2..10-3,2..10-3];
//var u_m1: [D] real;

config const numLocales:int;
const LocaleSpace = {0..numLocales-1};
const Locales: [LocaleSpace] locale;
const Dom: domain(2) dmapped Block(boundingBox={1..4, 1..8})
= {1..4, 1..8};

writeln("D:", Dom);
var A: [Dom] int;

    forall a in A do
      a = a.locale.id;

    writeln(A);
//writeln("innerD:", innerD);
//writeln("u:", u_m1);
