#include <mpi.h>
#include <stdio.h>
int main(int argc, char **argv)
{
int numtasks, rank, dest, source, rc, count, tag=1;
char outmsg[12]="Hello,world!";
char inmsg[12];
int root=0; 
MPI_Status Stat;
int numElements=2000;

MPI_Init(&argc,&argv);
MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
MPI_Comm_rank(MPI_COMM_WORLD, &rank);
// clean up
int local_sum=0;
int global_sum;
int i;

for (i =rank+1; i <= numElements; i += numtasks)
    local_sum =local_sum+i;


MPI_Reduce(&local_sum, &global_sum, 1, MPI_INT, MPI_SUM, 0,
           MPI_COMM_WORLD);

if (rank == 0) {
  printf("Total sum = %d\n", global_sum);
}


MPI_Finalize();
return 0;
}