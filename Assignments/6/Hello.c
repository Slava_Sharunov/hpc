#include "stdio.h"
#include "mpi.h"

int main (int argc, char *argv[])
{
int numtasks, rank, dest, source, rc, count, tag=1;
char outmsg[12]="Hello,world!";
char inmsg[12];
int root=0; 
MPI_Status Stat;
MPI_Request reqs[2];
MPI_Status stats[2];

MPI_Init(&argc,&argv);
MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
MPI_Comm_rank(MPI_COMM_WORLD, &rank);



//------------------Blocking version----------------------------//
if (rank==numtasks-1)
  rc = MPI_Send(&outmsg, 12, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
else
  rc = MPI_Send(&outmsg, 12, MPI_CHAR, rank+1, tag, MPI_COMM_WORLD);

if (rank==0)
  rc = MPI_Recv(&inmsg, 12, MPI_CHAR, numtasks-1, tag, MPI_COMM_WORLD, &Stat);
else
  rc = MPI_Recv(&inmsg, 12, MPI_CHAR, rank-1, tag, MPI_COMM_WORLD, &Stat);
  printf("Task %d: Received %s array from task %d with tag %d \n",
         rank, inmsg, Stat.MPI_SOURCE, Stat.MPI_TAG);

//------------------NON-Blocking version----------------------------//
/*
if (rank==numtasks-1)
  rc = MPI_Isend(&outmsg, 12, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &reqs[0]);
else
  rc = MPI_Isend(&outmsg, 12, MPI_CHAR, rank+1, tag, MPI_COMM_WORLD, &reqs[0]);

if (rank==0)
  rc = MPI_Irecv(&inmsg, 12, MPI_CHAR, numtasks-1, tag, MPI_COMM_WORLD, &reqs[1]);
else
  rc = MPI_Irecv(&inmsg, 12, MPI_CHAR, rank-1, tag, MPI_COMM_WORLD, &reqs[1]);

MPI_Waitall(2, reqs, stats);
  rc = MPI_Get_count(&Stat, MPI_CHAR, &count);
  printf("Task %d: Received %s array from task %d with tag %d \n",
         rank, inmsg, stats[1].MPI_SOURCE, stats[1].MPI_TAG);

 */


//------------------Broadcast version----------------------------//    
/*MPI_Bcast(&outmsg, 12, MPI_CHAR, root, MPI_COMM_WORLD);
printf("Task %d: Received %s array from task %d \n",rank, outmsg, root);*/


MPI_Finalize();
}