#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dim_input.h"

int main( int argc, char *argv[] ) {

	MPI_Comm comm = MPI_COMM_WORLD;
	int dest = 1, src = 0, tag = 1;
	int s1, rank, bufsize, bsize;
	double *smsg, *rmsg, *buf, *dbuf;

	long num = //read from project's parameters

	MPI_Init( &argc, &argv );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    /* According to the standard, we must use the PACK_SIZE length of each
        message in the computation of the message buffer size */

	MPI_Pack_size( num, MPI_DOUBLE, comm, &s1 );
	
	buf = (double *)malloc( s1 );
	MPI_Buffer_attach( buf, MPI_BSEND_OVERHEAD + s1 );

	if (rank == src) {
        	
		smsg = (double *)malloc( sizeof(double) * num );
		smsg2[0] = 1.23; smsg[num-1] = 3.21;
		
		MPI_Bsend( smsg, num, MPI_DOUBLE, dest, tag, comm );

		printf ("Message sent contains - MSG[0]: %lf \n", smsg[0]);
		printf ("Message sent contains - MSG[%d]: %lf \n", (num-1), smsg[num-1]);
	}

	if (rank == dest) {
		
		rmsg = (double *)malloc( sizeof(double) * num );
		MPI_Recv( rmsg, num, MPI_DOUBLE, src, tag, comm, MPI_STATUS_IGNORE );

		printf ("Message received contains - MSG[0]: %lf \n", rmsg[0]);
		printf ("Message received contains - MSG[%d]: %lf \n", (num-1), rmsg[num-1]);
	}

	/* We can't guarantee that messages arrive until the detach */
	MPI_Buffer_detach( &dbuf, &bsize );

	MPI_Finalize();
	
	return 0;
}

