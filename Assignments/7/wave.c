#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <time.h>
#include "dim_input.h"
#include "util.h"

#ifndef M_PI
#   define M_PI 3.14159265358979323846
#endif

#define FALSE 0
#define TRUE  1

//#define WRITE_OUTPUT


/**
 * Calculate a linear index from (i,j,k):
 * IDX(i,j,k) = ...

#define IDX(i,j,k) -- TODO --

*/
 #define IDX(i,j,k) (x_max*(j+y_max*k)+i)

/**
 * Write a cross section of the solution in u to a file.
 */
void write_grid(float* u, int timestep, int x_max, int y_max, int z_max)
{
	int i, j;
	char szFilename[255];
	sprintf (szFilename, "%04d.txt", timestep);
	printf ("Writing file %s...\n", szFilename);
	FILE* file = fopen (szFilename, "w");

	const int k = z_max / 3;
	for (j = 0; j < y_max; j++)
	{
		for (i = 0; i < x_max; i++)
			fprintf (file, "%f ", u[IDX(i,j,k)]);
		fprintf (file, "\n");
	}

	fclose (file);
}

int malloc_error (const char* err)
{
	fprintf (stderr, "Failed to allocate the field %s.\n", err);
	return EXIT_FAILURE;
}

/**
 * Determines whether n is a power of 2.
 */
int is_power_of_2 (int n)
{
	int i;
	for (i = n; i >= 2; i >>= 1)
	if (i & 1)
		return FALSE;
	return TRUE;
}

int main (int argc, char** argv)
{
	/* Stencil related varaibles */
	int i, j, k, t;
	double nFlops, GFlops;
	double time = 0.0;

	float* u_0_m1 = NULL;
	float* u_0_0 = NULL;
	float* u_0_1 = NULL;
	
	int x_max = X_MAX + 4; //READ X_MAX from dim_input
	int y_max = Y_MAX + 4; //READ Y_MAX from dim_input
	int z_max = Z_MAX + 4; //READ Z_MAX from dim_input

	const int T_MAX = 100;
	const float MIN = -1.f;
	const float MAX = 1.f;
	const float DX = (MAX - MIN) / (x_max - 3);
	const float DT = DX / 2.0f;
    
	const float DT_DX_SQUARE = DT * DT / (DX * DX);
	
	/* MPI related variables */
	int p, np, nprocs, myid, src, dst;
	int dims[3] = { 1, 1, 1 };
	int periods[3] = { FALSE, FALSE, FALSE };
	int coords[3];
	int steps[3];
	MPI_Comm comm;
	MPI_Datatype type;
    
	MPI_Request reqs[6];
	MPI_Status stats[6];
	int tag=0;

	MPI_Init (&argc, &argv);
	MPI_Comm_size (MPI_COMM_WORLD, &nprocs);
	MPI_Comm_rank (MPI_COMM_WORLD, &myid);
    
   
   
	/* #procs must be a power of 2 */
	if (!is_power_of_2 (nprocs)) 
	{
		fprintf (stderr, "#procs must be a power of 2!\n");
		MPI_Finalize ();
		return EXIT_FAILURE;
	}
 
	/* initialize grid dimensions in a straight forward manner */
	/**Here there is a space for future improving, we can use exactly 3-d separation in each dimension*/
	np = nprocs;
	for (p = 0; !(np & 1);  np >>= 1)
		dims[p] <<= 1;

	/* create the new communicator */
	printf ("Creating a cartesian grid with (%d, %d, %d) processes...\n", dims[0], dims[1], dims[2]);   
	MPI_Cart_create (MPI_COMM_WORLD, 3, dims, periods, FALSE, &comm);
     
	/* how are ranks mapped to coordinates in the MPI grid? */
	MPI_Cart_coords (comm, myid, 3, coords);
	printf ("myid=%d is mapped to (%d, %d, %d)\n", myid, coords[0], coords[1], coords[2]);
	MPI_Barrier (comm);
     
	/* Which ranks are my left and right (direction=0) neighbors? */
	MPI_Cart_shift (comm, 0, 1, &src, &dst);
	printf ("myid=%d, source (left neighbor) = %d, dest (right neighbor) = %d\n", myid, src, dst);
	MPI_Barrier (comm);
    
    /*in case of 3-d separation these conditions can be used full. Right now we initialize just a step in X-direction,*/
    /* Y and Z are still the same*/
	if (dims[0]>1)
		steps[0]=X_MAX/nprocs;
	else
		steps[0]=X_MAX;	
	if (dims[1]>1)
		steps[1]=Y_MAX/nprocs;
	else
		steps[1]=Y_MAX;	
	if (dims[2]>1)
		steps[2]=Z_MAX/nprocs;
	else
		steps[2]=Z_MAX;		

    float buffer_m_1[(steps[1])*(steps[2])];
    float buffer_m_2[(steps[1])*(steps[2])];
    float in_buffer_m_1[(steps[1])*(steps[2])];
    float in_buffer_m_2[(steps[1])*(steps[2])];
    float buffer_last_edge[(steps[1])*(steps[2])];
    float in_buffer_last_edge[(steps[1])*(steps[2])];
    MPI_Type_contiguous((steps[1])*(steps[2]), MPI_FLOAT, &type );
    MPI_Type_commit(&type);
	/* allocate memory */
	//TODO
	u_0_m1 = (float*) malloc (x_max * y_max * z_max * sizeof (float));
    if (u_0_m1 == NULL)
        return malloc_error ("u_0_m1");
    u_0_0 = (float*) malloc (x_max * y_max * z_max * sizeof (float));
    if (u_0_0 == NULL)
    {
        free (u_0_m1);
        return malloc_error ("u_0_0");
    }
    u_0_1 = (float*) malloc (x_max * y_max * z_max * sizeof (float));
    if (u_0_1 == NULL)
    {
        free (u_0_m1);
        free (u_0_0);
        return malloc_error ("u_0_1");
    }

	/* initialize the first timesteps */
	//TODO
	memset (u_0_m1, 0, x_max * y_max * z_max * sizeof (float));
	memset (u_0_0, 0, x_max * y_max * z_max * sizeof (float));
	memset (u_0_1, 0, x_max * y_max * z_max * sizeof (float));
  
  	for (k = 2; k < z_max - 2; k++)
    {
		for (j = 2; j < y_max - 2; j++)
		{
		      
          if (dst>0)
          {
    		for (i = 2+(myid*steps[0]); i <(myid+1)*steps[0]+4; i++)
			{
				float x = (i - 1) * DX + MIN;
				float y = (j - 1) * DX + MIN;
				float z = (k - 1) * DX + MIN;
						
                
	        	u_0_0[IDX(i,j,k)] = (float) (sin (2 * M_PI * x) * sin(2 * M_PI * y) * sin(2 * M_PI * z));
	         	u_0_m1[IDX(i,j,k)] = u_0_0[IDX(i,j,k)];
			}
          }
          else
          for (i = 2+(myid*steps[0]); i <(myid+1)*steps[0]+2; i++)
			{
				float x = (i - 1) * DX + MIN;
				float y = (j - 1) * DX + MIN;
				float z = (k - 1) * DX + MIN;
						
                
	        	u_0_0[IDX(i,j,k)] = (float) (sin (2 * M_PI * x) * sin(2 * M_PI * y) * sin(2 * M_PI * z));
	         	u_0_m1[IDX(i,j,k)] = u_0_0[IDX(i,j,k)];
			}
		}
	}
	
	MPI_Barrier(comm);

	
    	 

#ifdef WRITE_OUTPUT
	write_grid(u_0_0, 0, x_max, y_max, z_max);
#endif

	if ( myid == 0 ) {
		start_timing();
	}

	// declare some variables we will use in the compute block
    float c1 = 2.0f - DT_DX_SQUARE * 7.5f;
    float c2 = DT_DX_SQUARE * 4.0f / 3.0f;
    float c3 = DT_DX_SQUARE * (-1.0f/ 12.0f);
   
   
	/* START CALCULATION */
	for (t = 0; t < T_MAX; t++) {
        
	/* EXCHANGE BORDERS */
	//TODO
	if (dst>0)
	{
		int ii=0;
    	for (k = 2; k < z_max - 2; k++)
    	{
			for (j = 2; j < y_max - 2; j++)
			{
		        int i = (myid+1)*steps[0]+1;
    	 		buffer_m_1[ii]=u_0_0[IDX(i,j,k)];
    	 		i=(myid+1)*steps[0];
    	 		buffer_m_2[ii]=u_0_0[IDX(i,j,k)];
    	 		ii++;
    	 	}
    	 }	
    	   MPI_Isend(&buffer_m_1, 1, type, dst, 123, MPI_COMM_WORLD,&reqs[0]);
           MPI_Isend(&buffer_m_2, 1, type, dst, 124, MPI_COMM_WORLD,&reqs[1]);
           MPI_Wait(&reqs[0], &stats[0]);
           MPI_Wait(&reqs[1], &stats[1]);
   	}
   	
   	if (src >= 0)
   	{
   		MPI_Irecv(&in_buffer_m_1, 1, type, src, 123, MPI_COMM_WORLD,&reqs[2]);
        MPI_Irecv(&in_buffer_m_2, 1, type, src, 124, MPI_COMM_WORLD,&reqs[3]);
        MPI_Wait(&reqs[2], &stats[2]);
		MPI_Wait(&reqs[3], &stats[3]);
		
   		int in_ii=0; 
    	for (k = 2; k < z_max - 2; k++)
    	{
			for (j = 2; j < y_max - 2; j++)
			{
    	 	    int i=myid*steps[0]+1;
    	 		u_0_0[IDX(i,j,k)]=in_buffer_m_1[in_ii];
    	 		i=myid*steps[0];
    	 		u_0_0[IDX(i,j,k)]=in_buffer_m_2[in_ii];
    	 		in_ii++;
    	 	}
    	} 
           
   		} 

   		MPI_Barrier (comm);
	
		/* COMPUTE BLOCK */
	for (k = 2; k < z_max - 2; k++)
    {
		for (j = 2; j < y_max - 2; j++)
		{
    		for (i = 2+(myid*steps[0]); i <(myid+1)*steps[0]+2; i++)
    		{
                    u_0_1[IDX(i,j,k)] = c1*u_0_0[IDX(i,j,k)] - u_0_m1[IDX(i,j,k)]+
    				c3*(u_0_0[IDX(i-2,j,k)]+u_0_0[IDX(i,j-2,k)]+u_0_0[IDX(i,j,k-2)]+
    				u_0_0[IDX(i+2,j,k)]+u_0_0[IDX(i,j+2,k)]+u_0_0[IDX(i,j,k+2)])+
    				c2*(u_0_0[IDX(i-1,j,k)]+u_0_0[IDX(i,j-1,k)]+u_0_0[IDX(i,j,k-1)]+
    				u_0_0[IDX(i+1,j,k)]+u_0_0[IDX(i,j+1,k)]+u_0_0[IDX(i,j,k+1)]);
    		}
    	}
    }
    	
         
      	        
		/* UPDATE GRIDS */
		float* tmp = u_0_m1;
		u_0_m1 = u_0_0;
		u_0_0 = u_0_1;
		u_0_1 = tmp;
   		
	}

	if (myid==nprocs-1 && nprocs>1)
   	{
   		int ii=0;
    	for (k = 2; k < z_max - 2; k++)
    	{
			for (j = 2; j < y_max - 2; j++)
			{
		        int i = (myid+1)*steps[0]+1;
    	 		buffer_last_edge[ii]=u_0_0[IDX(i,j,k)];
    	 		i=(myid+1)*steps[0];
    	 		buffer_last_edge[ii]=u_0_0[IDX(i,j,k)];
    	 		ii++;
    	 	}
    	 }	
    	   MPI_Isend(&buffer_last_edge, 1, type, 0, 123, MPI_COMM_WORLD,&reqs[4]);
           MPI_Wait(&reqs[4], &stats[4]);       
   	}

   	if (myid==0 && nprocs>1)
   	{
   		MPI_Irecv(&in_buffer_last_edge, 1, type, nprocs-1, 123, MPI_COMM_WORLD,&reqs[5]);
        MPI_Wait(&reqs[5], &stats[5]);
		
		
   		int in_ii=0; 
    	for (k = 2; k < z_max - 2; k++)
    	{
			for (j = 2; j < y_max - 2; j++)
			{
    	 	    int i=myid*steps[0]+1;
    	 		u_0_0[IDX(i,j,k)]=in_buffer_last_edge[in_ii];
    	 		i=myid*steps[0];
    	 		u_0_0[IDX(i,j,k)]=in_buffer_last_edge[in_ii];
    	 		in_ii++;
    	 	}
    	} 
           
   	}  
	
	/* END CALCULATION */

	if ( myid == 0 ) {

		time = stop_timing();

#ifdef WRITE_OUTPUT
		write_grid(u_0_0, T_MAX-1, x_max, y_max, z_max);
#endif

		/* print statistics */    
		nFlops = (double) (X_MAX)*(double) (Y_MAX)*(double) (Z_MAX)*T_MAX*16.0;//... --TODO--;
		printf ("FLOPs in stencil code:      %e\n", nFlops);    
		printf ("Time spent in stencil code: %f\n", time);

		GFlops = nFlops / ( 1e9 * time );

		/* information read by the tool for printing graphs*/
		printMetric("GFlop/s", GFlops );
	}
    
	/* clean up */
	free (u_0_m1);
	free (u_0_0);
	free (u_0_1);

	MPI_Type_free (&type);
	
	MPI_Finalize ();
	return EXIT_SUCCESS;
}

