#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

#define SIZELIMIT 66600
#define START 0
#define STEP 1

int main(argc,argv)
int argc;
char *argv[];
{

  int numtasks, rank, i, tag=111, len, size;
  char name[MPI_MAX_PROCESSOR_NAME];

  char *data;
  MPI_Status status;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  printf ("MPI task %d started...\n", rank);

  data = (char *) malloc( SIZELIMIT );

  MPI_Get_processor_name(name, &len);

  /* Initialize */
  for( i=0; i<SIZELIMIT; i++) {
    data[i] =  'x';
  }

  size = START;
  //printf("%ld\n",sizeof(int));

  while (size <= SIZELIMIT) {
//    printf("My rank is: %d ready to send and receive from: %d\n", rank, neighbour);
    MPI_Send(data, size, MPI_BYTE, ((rank+1) % numtasks), tag, MPI_COMM_WORLD);
    MPI_Recv(data, size, MPI_BYTE, ((rank-1) % numtasks), tag, MPI_COMM_WORLD, &status);
    printf("RANK: %d \tHOST: %s \tSENT: %d BYTE\n", rank, name, size);fflush(stdout);
    size = size + STEP;
  }

  MPI_Finalize();
}

