set term postscript color
set title "OpenMP Experiments for Matrixes: Cache Misses with 1..16 Threads"
set yrange [0:0.1]
set xrange [0:20]
set xlabel "Number of Threads"
set ylabel "Data cache miss ratio"
set key left top


plot \
    "MissesCache.txt" using 1:2 title "NaiveMatrix" with linespoints, \
    "MissesCache.txt" using 1:3 title "BlockMatrix" with linespoints, \
    
