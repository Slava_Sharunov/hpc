#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>

static long N = 100000000;

main()
{
 int i, s; 
 unsigned short xi[3]; 
 double x,y; 
 double pi; 
 double w_time;
 w_time= omp_get_wtime();
 #pragma omp parallel
 {
 xi[0] = 1; /* These statements set up the random seed */
 xi[1] = 1;
 xi[2] = omp_get_thread_num();
 s = 0;
 #pragma omp for firstprivate(xi) private(x,y) reduction(+:s)
 for (i = 0; i < N; i++)
 {
 x = erand48(xi);
 y = erand48(xi);
 if (sqrt(x*x + y*y) <= 1.0) s++;
 }
 }
 w_time= omp_get_wtime()-w_time;
 pi = 4.0 * (double)s / (double)N;
 printf ("Execution time: %f\n", w_time);
 printf("Estimate of pi: %7.5f\n", pi);
}