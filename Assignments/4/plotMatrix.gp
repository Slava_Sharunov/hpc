set term postscript color
set title "OpenMP Experiments for Matrixes: Speedups with 1..16 Threads"
set xlabel "Number of Threads"
set ylabel "Seconds"
set key left top


plot \
    "speedMatrix.txt" using 1:2 title "NaiveMatrix" with linespoints, \
    "speedMatrix.txt" using 1:3 title "BlockMatrix" with linespoints, \
    
