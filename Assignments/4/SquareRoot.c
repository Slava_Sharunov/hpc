#include <stdio.h>
#include <omp.h>
#include <math.h>

static long N = 100000000;
double drand48();

int main()
{
	int i;
	int s=0;
	double x,y;
	double w_time;
	w_time= omp_get_wtime();

	#pragma omp parallel for private(x,y) reduction(+:s)
	for (i = 0; i < N; i++)
	{
		x=drand48();
		y=drand48();
		s+= hypot(x,y)<=1.0;
	}	
	w_time= omp_get_wtime()-w_time;
	printf("%f -- surprised?\n",4.0*(double)s/(double)N);
	printf ("Execution time: %f\n", w_time);
	return 0;
}

