#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

/* OS X */
#ifdef __MACH__
	#include <mach/mach_time.h>

	static const int GIGA = 1000000000;
	static mach_timebase_info_data_t sTimebaseInfo;

	void get_clockres(struct timespec* t) {
		if (sTimebaseInfo.denom == 0) {
			mach_timebase_info(&sTimebaseInfo);
		}
		t->tv_nsec = (uint64_t) (sTimebaseInfo.numer / sTimebaseInfo.denom);
	}

	void get_time(struct timespec* t) {
		uint64_t time = mach_absolute_time();
		time *= (sTimebaseInfo.numer / sTimebaseInfo.denom);
		t->tv_sec = time / GIGA;
		t->tv_nsec = time % GIGA;
	}

/* linux */
#elif __gnu_linux__
	void get_time(struct timespec* t) {
		clock_gettime(CLOCK_MONOTONIC, t);
	}
	void get_clockres(struct timespec* t) {
		clock_getres(CLOCK_MONOTONIC, t);
	}
#endif

void multiply(int n, double ** a, double ** b, double ** c);
void blockMultiply(int n, int bn, double ** a, double ** b, double ** c);
void fillMatrix(int n, double ** matrix);
void printMatrix(int n, double ** matrix);
double ** createMatrix(int n);
int MyArray[] = {10, 75, 150, 350, 500, 800, 1000, 1200, 1400, 1500};

int main(int argc, char * argv[]) {

	unsigned int mSize = 0, bSize = 0, runs, i; 
	struct timespec t1, t2, dt;
	double time, flops, gFlops;
	double ** a, ** b, ** c;
	int iterator;

    /*if (argc == 3 && isdigit(argv[1][0]) && isdigit(argv[2][0])) {
        mSize = atoi(argv[1]);
        bSize = atoi(argv[2]);
    }else {
        printf("USAGE\n   mult [SIZE] [BLOCKSIZE]\n");
        return 0;
    }*/
    if (argc == 2 && isdigit(argv[1][0])) {
        bSize = atoi(argv[1]);
    }else {
        printf("USAGE\n   mult [SIZE] [BLOCKSIZE]\n");
        return 0;
    }

    iterator = 0;
    while (iterator<10)
    {
    mSize=MyArray[iterator];
	get_clockres(&t1);
	printf("Timer resolution is %lu nano seconds.\n",t1.tv_nsec);

	a = (double**)createMatrix(mSize);
	b = (double**)createMatrix(mSize);
	c = (double**)createMatrix(mSize);

	fillMatrix(mSize, a);
	fillMatrix(mSize, b);

	flops = (double)mSize * (double)mSize * (double)mSize * 2.0;

	printf("Starting benchmark with mSize = %i and bSize = %i.\n",mSize,bSize);

	get_clockres(&t1);
	printf("Timer resolution is %lu nano seconds.\n",t1.tv_nsec);

	runs = time = 0;

	while (runs < 5) {

	    for (i = 0; i < mSize*mSize; i++) {
	            c[0][i] = 0;
	    }

		get_time(&t1);

	    if (bSize == 0)
	        multiply(mSize, a, b, c);
	    else
	        blockMultiply(mSize, bSize, a, b, c);

	    get_time(&t2);

	    if ((t2.tv_nsec - t1.tv_nsec) < 0) {
	        dt.tv_sec = t2.tv_sec - t1.tv_sec - 1;
	        dt.tv_nsec = 1000000000 - t1.tv_nsec + t2.tv_nsec;
	    }else {
	        dt.tv_sec = t2.tv_sec - t1.tv_sec;
	        dt.tv_nsec = t2.tv_nsec - t1.tv_nsec;
	    }

	    time += dt.tv_sec + (double)(dt.tv_nsec)*0.000000001;
	    runs ++;
	}

	gFlops = (flops/(1e9*time))*runs;
	printf("MATRIX SIZE: %i, GFLOPS: %f, RUNS: %i, TIME: %f\n",mSize, gFlops, runs,(time/runs));
	//printf ("Mean execution time: %f\n", (time/runs));
	free(a[0]);
	free(b[0]);
	free(c[0]);
	iterator++;
	}
}


void multiply(int n, double ** a, double ** b, double ** c) {
	//TODO

int i,j,k;
for(i=0;i<n;i++)
for(j=0;j<n;j++)
for(k=0;k<n;k++)
c[i][j] = c[i][j] + a[i][k]*b[k][j];

//-----------------------------------------
/*
double summand;
for(i=0;i<n;i++)
for(j=0;j<n;j++)
{
summand=0.0;
for(k=0;k<n;k++)
summand += a[i][k] * b[k][j];
c[i][j] = summand;
}
*/

//--------------------------------------
/*
double r;
for(k=0;k<n;k++)
for(i=0;i<n;i++)
{
r=a[i][k];
for(j=0;j<n;j++)
c[i][j] += r*b[k][j];
}
*/

//---------------------------------------
/*
double ** BT;
int i,j,k;
BT = (double**)createMatrix(n);
for (i = 0; i < n; i++)
for (j = 0; j < n; j++)
BT[j][i] = b[i][j];
for (i = 0; i < n; i++) {
for (j = 0; j < n; j++) {
double summand = 0.0;
for (k = 0; k < n; k++) {
summand += a[i][k] * BT[j][k];
}
c[i][j] = summand;
}
}
*/


}


void blockMultiply(int n, int bn, double ** a, double ** b, double ** c) {
	//TODO

int i, j, k, kk, jj;
int en = bn * (n/bn);
for (i = 0; i < n; i++)
for (jj = 0; jj< en; jj += bn)
for (kk= 0; kk < en; kk += bn)

for (j = jj; j < jj + bn; j++)
for (k = kk; k < kk + bn; k++)
c[i][j] = c[i][j]+a[i][k]*b[k][j];


//-----------------------------------

/*
int i, j, k, kk, jj;
double sum;
int en = bn * (n/bn);
double ** BT;
BT = (double**)createMatrix(n);
for (i = 0; i < n; i++)
for (j = 0; j < n; j++)
BT[j][i] = b[i][j];
for (jj = 0; jj < en; jj += bn)
for (kk = 0; kk < en; kk += bn)
for (i = 0; i < n; i++)
for (j = jj; j < jj + bn; j++)
{
sum = 0.0;
for (k = kk; k < kk + bn; k++)
sum += a[i][j]*BT[j][k];
c[i][j] = sum;
}
*/
}


double ** createMatrix(int n) {
	int i;
	double ** matrix = (double**) calloc(n,sizeof(double*));
	double * m = (double*) calloc(n*n,sizeof(double));
	for (i = 0; i < n; i++) {
		matrix[i] = m+(i*n);
	}
	return matrix;
}

void fillMatrix(int n, double ** matrix) {
	int i;
	for (i = 0; i < n*n; i++) {
		matrix[0][i] = (rand()%10) - 5; //between -5 and 4
	}
}

//print the matrix by row
void printMatrix(int n, double ** matrix) {
	int i, j;

	printf("{");
	for (i = 0; i < n; i++) {
		printf("[");
		for (j = 0; j < n; j++) {
			printf("%d",(int)matrix[i][j]);
			if (j != n-1)
				printf(",");
			else
				printf("]");
		}
		if (i != n-1)
			printf(",\n");
	}
	printf("}\n");
}

