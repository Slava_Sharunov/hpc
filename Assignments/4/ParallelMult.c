#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>

static int n = 1024;

void printMatrix(int ** jj)
{
	int i,j;
	for (i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
			printf("%d ",jj[i][j]);
		printf("\n");
	}

}


void multiply(int ** a, int ** b, int ** c)
{
	int summand;
	int i,j,k;
	#pragma omp parallel for private(i,j,k) reduction (+:summand)
    for(i=0;i<n;i++)
		for(j=0;j<n;j++)
			{
				summand=0;
				for(k=0;k<n;k++)
					summand += a[i][k] * b[k][j];
					c[i][j] = summand;
			}
}

void blockmultiply(int ** a, int ** b, int ** c, int blockSize)
{
	int kk, jj,i,j,k;
	int bn =blockSize;
	int en = bn * (n/bn);


#pragma omp parallel for private(i,j,k,kk,jj)
	/*-----------------Variant#2------------------*/
//int sum;	
/*#pragma omp parallel for private(i,j,k,kk,jj) reduction (+:sum)
for (i = 0; i < n; i++)
for (jj = 0; jj< en; jj += bn)
for (kk= 0; kk < en; kk += bn)

for (j = jj; j < jj + bn; j++)
for (k = kk; k < kk + bn; k++)
{
	sum=0;
	for(k=0;k<n;k++)
		sum+= a[i][k] * b[k][j];
		c[i][j] = sum;
}*/

for (i = 0; i < n; i++)
for (jj = 0; jj< en; jj += bn)
for (kk= 0; kk < en; kk += bn)

for (j = jj; j < jj + bn; j++)
for (k = kk; k < kk + bn; k++)
c[i][j] = c[i][j]+a[i][k]*b[k][j];
}


int ** createMatrix(int n) {
	int i;
	int ** matrix = (int**) calloc(n,sizeof(int*));
	int * m = (int*) calloc(n*n,sizeof(int));
	for (i = 0; i < n; i++) {
		matrix[i] = m+(i*n);
	}
	return matrix;
}

int main(int argc, char * argv[])
{
	int i,j,k=1;
	int ** a, ** b, ** c;
	int bSize=0;
	double w_time;

	if (argc == 2 && isdigit(argv[1][0])) {
        bSize = atoi(argv[1]);
    }else {
        printf("USAGE\n   mult [SIZE] [BLOCKSIZE]\n");
        return 0;
    }

    a = (int**)createMatrix(n);
	b = (int**)createMatrix(n);
	c = (int**)createMatrix(n);

	for(i=0;i<n;i++)
      for(j=0;j<n;j++)
      	a[i][j]=k++;

    for(i=0;i<n;i++)
      for(j=0;j<n;j++)
      	b[i][j]=k--; 

w_time= omp_get_wtime();
	    if (bSize == 0)
	        multiply(a, b, c);
	    else
	        blockmultiply(a, b, c,bSize);
w_time=omp_get_wtime() - w_time;


  /*printf("Matrix a\n");
    printMatrix(a);
    printf("\n"); 
     printf("Matrix b\n");
    printMatrix(b);
    printf("\n"); 
     printf("Matrix c\n");
    printMatrix(c);
    printf("\n");*/
    printf ("Execution time: %f\n", w_time); 
    free(a[0]);
	free(b[0]);
	free(c[0]); 
	return 0;
}
