#define _USE_MATH_DEFINES
#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>

static long N = 100000000;

void main ()
{
int i;
double w_time;

double* a=(double*)malloc(N*sizeof(double));

w_time= omp_get_wtime();
//TODO
#pragma omp parallel for
for(i=0;i<N;i++)
   a[i] = sin (2.0 * M_PI /1000.0 * i);
w_time=omp_get_wtime() - w_time;

    printf ("Execution time: %f\n", w_time);
    free(a);
}