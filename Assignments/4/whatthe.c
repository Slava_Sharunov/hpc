#include <stdio.h>
#include <omp.h>

static const int a[]={1,2,3,4,5,6,7,8,9,10,11};

int sum(const int* arr,int size)
{
	int sum=0,i;
	#pragma omp parallel for reduction(+:sum)
    for (i = 0; i < size; i++) {
         sum += a[i];
    }
	
	return sum;
}

int main()
{
	int summand;
	double w_time;
	w_time= omp_get_wtime();
	summand=sum(a,11);
	w_time= omp_get_wtime()-w_time;
	printf("sum: %d\n",summand);
	printf ("Execution time: %f\n", w_time);
	return 0;
}