set term postscript color
set title "OpenMP Experiments: Speedups with 1..16 Threads"
set xtics (1, 2, 4, 8, 16)
set ytics (1, 2, 4, 8, 16)
set xlabel "Number of Threads"
set ylabel "Speedup vs. Sequential Version"
set key left top


plot \
    "speedup.txt" using 1:2 title "Linear Speedup" with linespoints, \
    "speedup.txt" using 1:3 title "Sines" with linespoints, \
    "speedup.txt" using 1:4 title "What the heck..." with linespoints, \
    "speedup.txt" using 1:5 title "Numbers" with linespoints, \
    "speedup.txt" using 1:6 title "Bunnies" with linespoints, \

