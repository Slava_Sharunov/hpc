#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>

static long N = 512;

#define NUM_THREADS 4

int main()
  {
    int i;
    double* a=(double*)malloc(N*sizeof(double));
    a[0]=1; a[1]=1;
  
 double w_time;
 w_time= omp_get_wtime();
 #pragma omp parallel for
   for (i = 2; i < N; i++)
      a[i] = a[i-1] + a[i-2];
 w_time= omp_get_wtime()-w_time;
  printf ("Execution time: %f\n", w_time);
   printf("%f\n",a[18]);
   free(a);

    return 0;
  }
