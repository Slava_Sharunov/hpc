#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

void get_time(struct timespec* t) {
	clock_gettime(CLOCK_MONOTONIC, t);
}
void get_clockres(struct timespec* t) {
	clock_getres(CLOCK_MONOTONIC, t);
}

void transpose(double **m, double **mt, int rows, int cols);
void transposeBlock(double **m, double **mt, int rows, int cols, int block);
double** createMatrix(int rows, int cols);
void print(double **m, int rows, int cols);
void fillMatrix(double **m, int rows, int cols);

int main(int argc, char * argv[]) {
	unsigned int rows = 0, cols = 0, bsize;
	struct timespec t1, t2, dt;
	double time;
	double **a, **b;
	
	if (argc == 4 && isdigit(argv[1][0]) && isdigit(argv[2][0]) && isdigit(argv[3][0])) {
        rows = atoi(argv[1]);
        cols = atoi(argv[2]);
        if ((rows <= 0) || (cols <=0)) {
        	printf("rows and cols > 0\n");
        	return 0;
        }
        bsize = atoi(argv[3]);
    } else {
        printf("USAGE\n   transpose [ROWS] [COLS] [BLOCKSIZE]\n");
        return 0;
    }
    
    get_clockres(&t1);
	printf("Timer resolution is %lu nano seconds.\n",t1.tv_nsec);
	
	a = (double**)createMatrix(rows, cols);
	b = (double**)createMatrix(cols, rows);
	
	fillMatrix(a, rows, cols);
	
	printf("Starting benchmark with rows = %i and cols = %i.\n", rows, cols);
	
	get_time(&t1);
	
	if (bsize == 0) {
		transpose(a, b, rows, cols);
	} else {
		transposeBlock(a, b, rows, cols, bsize);
	}
		
	get_time(&t2);

	if ((t2.tv_nsec - t1.tv_nsec) < 0) {
		dt.tv_sec = t2.tv_sec - t1.tv_sec - 1;
		dt.tv_nsec = 1000000000 - t1.tv_nsec + t2.tv_nsec;
	} else {
		dt.tv_sec = t2.tv_sec - t1.tv_sec;
		dt.tv_nsec = t2.tv_nsec - t1.tv_nsec;
	}

	time = dt.tv_sec + (double)(dt.tv_nsec)*0.000000001;
	
	printf ("Execution time: %f\n", time);
	
	free(a[0]);
	free(a);
	free(b[0]);
	free(b);
}

void transpose(double **m, double **mt, int rows, int cols) {
	//TODO
	int i,j;
	for(i=0;i<rows;i++)
		for(j=0;j<cols;j++)
			mt[j][i] = m[i][j];
}

void transposeBlock(double **m, double **mt, int rows, int cols, int block) {
	//TODO
	int i,j,k,l;
	for (i = 0; i < rows; i += block) 
   		for (j = 0; j < cols; j += block) 
        	for (k = i; k < i + block; ++k) 
            	for (l = j; l < j + block; ++l) 
                	mt[l][k] = m[k][l];            

            	
}

double** createMatrix(int rows, int cols) {
	int i;
	double ** matrix = (double**) calloc(rows,sizeof(double*));
	double * m = (double*) calloc(rows*cols,sizeof(double));
	for (i = 0; i < rows; i++) {
		matrix[i] = m+(i*cols);
	}
	return matrix;
}

void fillMatrix(double **m, int rows, int cols) {
	int i, j;
	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			m[i][j] = (rand()%10) - 5; //between -5 and 4
		}
	}
}

void print(double **m, int rows, int cols) {
	int i, j;
	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			printf("%f\t",m[i][j]);
		}
		printf("\n");
	}
}
