#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

void get_time(struct timespec* t) {
	clock_gettime(CLOCK_MONOTONIC, t);
}
void get_clockres(struct timespec* t) {
	clock_getres(CLOCK_MONOTONIC, t);
}

void naive_sum(double** m, int rows, int cols, double* res);
void better_sum(double** m, int rows, int cols, double* res);
void print(double *array, int n);
void printM(double **m, int rows, int cols);
void fillMatrix(double** m, int rows, int cols);
double** createMatrix(int rows, int cols);

int main(int argc, char * argv[]) {
	unsigned int dim = 0, rows, cols, mode = 0;
	struct timespec t1, t2, dt;
	double time = 0.0;
	double** a;
	double* array_sum;

	if (argc == 3 && isdigit(argv[1][0]) && isdigit(argv[2][0])) {
        dim = atoi(argv[1]);
        if (dim <= 0) {
        	printf("dim > 0\n");
        	return 0;
        }
		rows = cols = dim;
        mode = atoi(argv[2]); // [0 = naive] [1 = better sum]
    }else {
        printf("USAGE\n  sum [DIM] [MODE]\n");
        return 0;
    }

	get_clockres(&t1);
	printf("Timer resolution is %lu nano seconds.\n",t1.tv_nsec);

	a = (double**)createMatrix(rows, cols);
	array_sum = (double*) calloc(cols, sizeof(double));
	fillMatrix(a, rows, cols);
	
	printf("Starting benchmark with rows = %i and cols = %i.\n", rows, cols);

	get_time(&t1);

	if (mode == 0) {
		naive_sum(a, rows, cols, array_sum);
	} else {
		better_sum(a, rows, cols, array_sum);
	}
    
    get_time(&t2);

    if ((t2.tv_nsec - t1.tv_nsec) < 0) {
        dt.tv_sec = t2.tv_sec - t1.tv_sec - 1;
        dt.tv_nsec = 1000000000 - t1.tv_nsec + t2.tv_nsec;
    }else {
        dt.tv_sec = t2.tv_sec - t1.tv_sec;
        dt.tv_nsec = t2.tv_nsec - t1.tv_nsec;
    }

    time = dt.tv_sec + (double)(dt.tv_nsec)*0.000000001;
	

	printf ("Execution time: %f\n", time);

	free(a[0]);
	free(a);
	free(array_sum);
}


void naive_sum(double** m, int rows, int cols, double *res) {
	//TODO
	int i,j;
	double sum;
	for (j=0;j<cols;j++)
	{
		sum=0;
		for(i=0;i<rows;i++)
			sum = sum + m[i][j];
		res[j]=sum;
	}
}

void better_sum(double** m, int rows, int cols, double *res) {
	//TODO
	int i,j;
	double sum;
	for (i=0;i<rows;i++)
	{
		sum=0;
		for(j=0;j<cols;j++)
			sum = sum + m[i][j];
		res[i]=sum;
	}
}

double** createMatrix(int rows, int cols) {
	int i;
	double ** matrix = (double**) calloc(rows,sizeof(double*));
	double * m = (double*) calloc(rows*cols,sizeof(double));
	for (i = 0; i < rows; i++) {
		matrix[i] = m+(i*cols);
	}
	return matrix;
}

void fillMatrix(double** m, int rows, int cols) {
	int i, j;
	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			m[i][j] = (rand()%10) - 5; //between -5 and 4
		}
	}
}

void print(double *array, int n) {
	int i;
	printf("{ ");
	for (i = 0; i < n; i++) {
		printf("%f ",array[i]);
	}
	printf("}\n");
}

void printM(double **m, int rows, int cols) {
	int i, j;
	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			printf("%f\t",m[i][j]);
		}
		printf("\n");
	}
}
