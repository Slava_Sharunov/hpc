YTICS="128m 64m 32m 16m 8m 4m 2m 1024k 512k 256k 128k 64k 32k 16k 8k 4k 2k 1k "
XTICS="	s1	s3	s5	s7	s9	s11	s13	s15	s17	s19	s21	s23	s25	s27	s29	s31	"
set ticslevel 0
unset key
set title "Clock frequency is approx. 2294.9 MHz - Memory mountain (MB/sec)"
set zrange [0:10000]
unset surf
set style line 1 lt 4 lw .5
set pm3d at s hidden3d 1
set for [i=1:words(XTICS)] xtics ( word(XTICS,i) i-1 )
set for [i=1:words(YTICS)] ytics ( word(YTICS,i) i-1 )
set grid ztics back
splot "<awk '{$1=\"\"}1' mountain.dat | sed '1,3 d'" matrix with pm3d
