#!/bin/bash
# @author: Danilo Guerrera (University of Basel)
# HPC 15
#
file="mountain.dat"
gpfile="mountain.gp"

TITLE=`head -1 $file`" - "`head -2 $file | tail -1`

echo "YTICS=\"`sed '1,2 d' $file | awk 'BEGIN{getline}{printf \"%s \",$1}'`\"" > $gpfile
echo "XTICS=\"`head -3 $file | tail -1`\"" >> $gpfile

#echo "set terminal svg noenhanced" >> $gpfile
#echo "set output 'mountain.svg'" >> $gpfile
echo "set ticslevel 0" >> $gpfile
echo "unset key" >> $gpfile
echo "set title \"$TITLE\"" >> $gpfile
echo "set zrange [0:10000]" >> $gpfile
echo "unset surf" >> $gpfile
echo "set style line 1 lt 4 lw .5" >> $gpfile
echo "set pm3d at s hidden3d 1" >> $gpfile
echo "set for [i=1:words(XTICS)] xtics ( word(XTICS,i) i-1 )" >> $gpfile
echo "set for [i=1:words(YTICS)] ytics ( word(YTICS,i) i-1 )" >> $gpfile
echo "set grid ztics back" >> $gpfile
echo "splot \"<awk '{"'$1=\"\"'"}1' $file | sed '1,3 d'\" matrix with pm3d" >> $gpfile
gnuplot mountain.gp -persist
