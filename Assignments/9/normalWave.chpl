use dim_input; //it contains input parameters
use Time;

const PI: real = 3.1415926535;

//
// Program parameters; can be specified on the command line
//

config const
    // domain size
    x_max: int = D_SIZE+4,//read from project
    y_max = x_max,
    z_max = x_max,
    
    // number of timesteps
    t_max: int = T_MAX,//read from project
    
    // write result to files?
    write_output: bool = false;
 
proc IDX(i,j,k)
{
    return ((i)+(x_max)*((j)+(y_max)*(k)));
} 

proc write_data (u: [0..x_max*y_max*z_max] real, t)
{
    // create the file name
    var filename = t + ".txt";
    if (t < 10) then
        filename = "000" + filename;
    else if (t < 100) then
        filename = "00" + filename;
    else if (t < 1000) then
        filename = "0" + filename;


    //Create and open an output file with the specified filename in write mode
    var outfile = open(filename, iomode.cw);
    var writer = outfile.writer();
    
    // write the data
    // can be plotted with gnuplot: 'splot "./0001.txt" with pm3d'
    var k : int = z_max/3;
    for j in {2..y_max-3}
    {
        for i in {2..x_max-3}
        {
             //writer.write (u[.., .., z_max / 3 .. z_max / 3]); --old version
             writer.write (u[IDX(i,j,k)]);
        }
    }
    //close the file
    writer.close();
    outfile.close();
}

//
// Wave stencil benchmark.
//

proc main ()
{
	const
	    fMin: real = -1.0,
	    fMax: real = 1.0,
	    dx: real = (fMax - fMin) / ((x_max -3) : real),
	    dt: real = dx / (2.0),
	    dt_dx_sq: real = (dt * dt) / (dx * dx),
	    
	    c1: real = 2.0 - dt_dx_sq*7.5,
	    c2: real = dt_dx_sq*(4.0 / 3.0),
	    c3: real = dt_dx_sq*(-1.0 / 12.0);


    // create grids
    var D: domain(1) = 0..x_max*y_max*z_max;
    var u_m1: [D] real,//TODO
        u_0:  [D] real,//TODO
        u_1:  [D] real;//TODO

    // iterate on the grid and initialize using Chapel
    //TODO like in the C code


    //---------------------------Clarification------------------------------------//
    //C version of this condition was:
    //for (k=2;k<z-max-2;k++) => in Chapel we have to limit increasing of k till z_max-3
    //and so on in other loops
    //---------------------------Clarification------------------------------------//


    for k in 2..z_max-3
    {
        for j in 2..y_max-3
        {
            for i in 2..x_max-3
            {
                var x:real = (i - 1) * dx + fMin;
                var y:real = (j - 1) * dx + fMin;
                var z:real = (k - 1) * dx + fMin;
               
                u_0[IDX(i,j,k)] =  (sin(2 * PI * x) * sin(2 * PI * y) * sin(2 * PI * z));
                u_m1[IDX(i,j,k)] = u_0[IDX(i,j,k)];
            }
        }
    }

    
    // write the initial grid (a slice of it) to a file
    if (write_output) then
        write_data (u_0, 0);
    // do the stencil computation
    var clock = new Timer ();
    clock.start ();

    //start the actual computation here
    for t in 0 .. t_max-1
    {
        //iterate on the grid elements and update the value of u_1
        //TODO
        for k in 2..z_max-3
          {
            for j in 2..y_max-3
             {
                for i in 2..x_max-3
                {
                    u_1[IDX(i,j,k)] = c1*u_0[IDX(i, j, k)] - u_m1[IDX(i, j, k)]+
                        c2 * (u_0[IDX(i+1, j, k)] + u_0[IDX(i-1, j, k)] +
                              u_0[IDX(i, j+1, k)] + u_0[IDX(i, j-1, k)] +
                              u_0[IDX(i, j, k+1)] + u_0[IDX(i, j, k-1)])+
                        c3 * (u_0[IDX(i+2, j, k)] + u_0[IDX(i-2, j, k)] +
                              u_0[IDX(i, j+2, k)] + u_0[IDX(i, j-2, k)] +
                              u_0[IDX(i, j, k+2)] + u_0[IDX(i, j, k-2)]);
                }
            }
        }

        
        if (write_output) then
            write_data (u_1, t);
         
        //swap grids
        //TODO  
        var tmp: [D] real = u_m1;
        u_m1 = u_0;
        u_0 = u_1;
        u_1 = tmp;
    
    }
    clock.stop ();

    // print performance statistics
    var fFlops: real = (x_max : real) * (y_max : real) * (z_max : real) * (t_max : real) * 19.0; //check if your flops are 19
    writeln ("FLOPs in stencil code:      ", fFlops);
    writeln ("Time spent in stencil code: ", clock.elapsed ());
    writeln ("Performance in GFlop/s:     ", fFlops / (1e9 * clock.elapsed ()));
}

