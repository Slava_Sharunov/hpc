#!/bin/bash
app="$1"
jobfile="$1.job"


nodes="2"

cwd=`pwd`

# if file already exists, delete it first
[ -f $jobfile ] && rm $jobfile

echo "#PBS -N $app-$nodes" >> $jobfile
echo "#PBS -j oe" >> $jobfile
echo "#PBS -l nodes=$nodes" >> $jobfile
echo "#PBS -o $cwd/$1.out" >> $jobfile
echo "" >> $jobfile
echo 'echo "JOB is going to be executed on:"' >> $jobfile
echo "hostname" >> $jobfile
echo "" >> $jobfile

echo '/export/hpwc/HPC2016/software/easybuild/software/OpenMPI/1.10.1-GCC-4.9.3-2.25/bin/mpirun -hostfile $PBS_NODEFILE' -np $nodes --map-by node $app >> $jobfile

qsub $jobfile
