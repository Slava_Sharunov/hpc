use Random;

/*
 *  Prints 10 random values [0,1)
 */

config const seed = 12345;

var rs = new RandomStream(seed, parSafe=false); // parSafe: if random is parallel safe

for x in 1..10 do
    writeln(rs.getNext());

delete rs;
