use Random;

config const n: int = 100000;
config const seed = 1234567890;

var rs = new RandomStream(seed, parSafe=false);
var count=0;
for i in 1..n do
  if (rs.getNext()**2 + rs.getNext()**2) < 1 then
     count+=1;

writeln ("PI = ",count*4.0/n);     

delete rs;